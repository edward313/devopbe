FROM maven:3.8.4-openjdk-17-slim AS build

WORKDIR /app

# Copy mã nguồn ứng dụng Spring Boot vào thư mục /app trong container
COPY . /app

# Build ứng dụng Spring Boot với Maven (phải chạy từ thư mục chứa file pom.xml)
RUN mvn -f /app/pom.xml clean package

FROM openjdk:11

WORKDIR /app
COPY --from=build /app/target/demo-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT [ "java", "-jar" , "app.jar" ]